import csv
import numpy as np
from tensorflow import keras
# Loading table
from load_tests import tests, answers, weather

# Loading model
model = keras.models.load_model("./models/model_4")

tests = tests[-400:]
answers = answers[-400:]
print(f'Test rows: {len(tests)}')

np.set_printoptions(precision=3, formatter={'float_kind': '{:1.2f}'.format}, suppress=True)
tests = np.array(tests).astype('float32')
answers = np.array(answers).astype('float32')

result = model.predict(tests)
score = 0
scores = [0] * len(weather)
best_scores = [0] * len(weather)
for i in range(len(result)):
    # a = np.argmax(result[i])
    a = 0
    if result[i][1] >= result[i][2]:
        a = 1
    else:
        a = 2
    b = np.argmax(answers[i])
    best_scores[b] = best_scores[b] + 1
    if a == b:
        score = score + 1
        scores[a] = scores[a] + 1
    else:
        scores[a] = scores[a] - 1

score = score / len(result) * 100
print(f'Score: {score}%')

for i in range(len(weather)):
    print(f'{weather[i]}: {scores[i] / best_scores[i] * 100}%')

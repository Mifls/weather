import numpy as np
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dropout, Dense
from keras.optimizers import SGD, RMSprop, Adam

# Loading table
from load_tests import tests, answers, weather

tests = tests[:-400]
answers = answers[:-400]
print(f'Fit rows: {len(tests)}')

freq = [0] * len(weather)
for i in range(len(answers)):
    for j in range(len(weather)):
        freq[j] = freq[j] + answers[i][j]

for i in range(len(weather)):
    print(f'{weather[i]}: {freq[i]}')


def create_model():
    model = Sequential()
    model.add(Dense(18, input_shape=(tests.shape[1],), activation='sigmoid'))
    model.add(Dense(18, activation='relu'))
    model.add(Dropout(0.1))
    model.add(Dense(18, activation='sigmoid'))
    model.add(Dropout(0.1))
    model.add(Dense(len(weather), activation='sigmoid'))
    model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.01), metrics=['accuracy'])
    return model


def learn(model, input_table, answers, filename, batch_size=64, epochs=30):
    learning_rate_reduction = keras.callbacks.ReduceLROnPlateau(patience=3, verbose=1, factor=0.8, min_lr=1e-30)
    model.fit(input_table, answers, validation_data=(input_table, answers),
              callbacks=[learning_rate_reduction],
              batch_size=batch_size, epochs=epochs)
    model.save(filename)
    return model


marginal_tests = []
marginal_answers = []
for i in range(len(tests)):
    if answers[i].index(1) in [0, 3, 4]:
        marginal_tests.append(tests[i])
        marginal_answers.append(answers[i])

np.set_printoptions(precision=3, formatter={'float_kind': '{:1.2f}'.format}, suppress=True)
tests = np.array(tests).astype('float32')
answers = np.array(answers).astype('float32')
marginal_tests = np.array(marginal_tests).astype('float32')
marginal_answers = np.array(marginal_answers).astype('float32')

model = create_model()
# model = learn(model, tests, answers, 'models/model_1', batch_size=30, epochs=10)
# model = learn(model, tests, answers, 'models/model_2', batch_size=10, epochs=20)
# model = learn(model, tests, answers, 'models/model_3', batch_size=5, epochs=30)
# model = learn(model, tests, answers, 'models/model_4', batch_size=3, epochs=50)

model = learn(model, marginal_tests, marginal_answers, 'models/model_3', batch_size=1, epochs=30)
# model.optimizer = Adam(lr=0.5)
for i in range(30):
    # model.optimizer = Adam(lr=0.01)
    model = learn(model, marginal_tests, marginal_answers, 'models/model_3', batch_size=1, epochs=10)
    model = learn(model, tests, answers, 'models/model_4', batch_size=1, epochs=10)

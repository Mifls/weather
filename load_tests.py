import csv

table = []
with open('seattle-weather.csv', newline='') as csvfile:
    train = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in train:
        table.append(row)
        print(row)

del table[0]
print(f'Total rows: {len(table)}')


weather = (
    'drizzle',
    'rain',
    'sun',
    'snow',
    'fog'
)

seasons = [[31, 31, 28, 90],
           [31, 30, 31, 92],
           [30, 31, 31, 92],
           [30, 31, 30, 91]]


# [is_winter, part, is_spring, part, is_summer, part, is_fall, part,
# precipitation, temp_max, temp_min, temp_diff, wind]
def prepare_data(line):
    dateline = line[0].split('-')
    dateline[1] = int(dateline[1]) % 12
    season = dateline[1] // 4
    month = dateline[1] % 3
    day = int(dateline[2])
    for i in range(month):
        day += seasons[season][i]
    data = [0, 0, 0, 0, 0, 0, 0, 0,
            float(line[1]),
            float(line[2]), float(line[3]), float(line[2]) - float(line[3]),
            float(line[4])]
    data[season * 2] = 1
    data[season * 2 + 1] = day / seasons[season][3]
    ans = [0] * len(weather)
    ans[weather.index(line[5])] = 1
    return data, ans


tests = []
answers = []
for row in table:
    test, answer = prepare_data(row)
    tests.append(test)
    answers.append(answer)